let http = require("http");

http.createServer(function (req, res) {
    // Output "Welcome to the Booking System"
    if (req.url == "/" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to the Booking System");
    }
    // Output "Welcome your profile"
    if (req.url == "/profile" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome your profile");
    }
    // Output "Here's our courses available"
    if (req.url == "/courses" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Here's our courses available");
    }
    // Output "Add course to our resources"
    if (req.url == "/addCourse" && req.method == "POST") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Add course to our resources");
    }
    // Output "Update a course to our resources"
    if (req.url == "/updateCourse" && req.method == "PUT") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Update a course to our resources");
    }
    // Output "Archive course to our resources"
    if (req.url == "/archiveCourse" && req.method == "DELETE") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Archive course to our resources");
    }
}).listen(4000);

console.log("System is now runnng at localhost:4000");
